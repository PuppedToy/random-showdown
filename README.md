# Random Showdown

Random Showdown is a randomizer that sets up a server and a client for [Pokemon Showdown!](https://github.com/Zarel/Pokemon-Showdown) and randomizes them periodically. It keeps the game always updated by watching the official repository.